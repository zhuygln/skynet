%{
#include "Reactions/ReactionLibraryBase.hpp"
#include "Reactions/NeutrinoReactionLibrary.hpp"
%}

%include "Reactions/ReactionLibraryBase.hpp"
%include "Reactions/NeutrinoReactionLibrary.hpp"
