set(tinyxml2_SOURCES
  tinyxml2.cpp
)

add_SkyNet_library(tinyxml2 "${tinyxml2_SOURCES}")