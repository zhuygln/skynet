%{
#include "Utilities/FunctionVsTime.hpp"
#include "DensityProfiles/ConstantFunction.hpp"
%}

%ignore MakeUniquePtr;

%include "Utilities/FunctionVsTime.hpp"
%include "DensityProfiles/ConstantFunction.hpp"
