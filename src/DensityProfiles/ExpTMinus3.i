%{
#include "Utilities/FunctionVsTime.hpp"
#include "DensityProfiles/ExpTMinus3.hpp"
%}

%ignore MakeUniquePtr;

%include "Utilities/FunctionVsTime.hpp"
%include "DensityProfiles/ExpTMinus3.hpp"
